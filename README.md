# Redcap Vagrant

[Vagrant](http://www.vagrantup.com/) configuration and install scripts for running [RedCap](http://project-redcap.org) on an Ubuntu 64 Precise image.

mark change test The RedCap web application will be available at `http://localhost:8080/redcap`.  The source will be at `/usr/local/redcap/`.
The phpMyAdmin web application will be available at `http://localhost:8080/pma`.  The source will be at `/usr/local/pma/`.

The box will boot and install RedCap and dependencies.  This will take several minutes for the initial install.

## Install

~~~
$ git clone https://github.com/???/redcap-vagrant.git redcap-vagrant
$ cd redcap-vagrant
$ vagrant up
~~~

## Notes
 * This is intended for development only.  Change passwords if you intend to use this config for deployment.
 * Your RedCap environment will be dropped and reconfigured anytime that `vagrant reload` or `vagrant provision` is run.
 Be sure to backup any data or code before running these commands.
 * Various other development tools, mainly Python, are installed too.  Comment those out if they are not needed.
